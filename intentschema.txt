{
    "intents": [
        {
            "intent": "AMAZON.PauseIntent"
        },
        {
            "intent": "AMAZON.ResumeIntent"
        },
        {
            "intent": "AMAZON.NextIntent"
        },
        {
            "intent": "AMAZON.PreviousIntent"
        },
        {
            "intent": "AMAZON.LoopOnIntent"
        },
        {
            "intent": "AMAZON.LoopOffIntent"
        },
        {
            "intent": "AMAZON.ShuffleOnIntent"
        },
        {
            "intent": "AMAZON.ShuffleOffIntent"
        },
        {
            "intent": "AMAZON.RepeatIntent"
        },
        {
            "intent": "AMAZON.StartOverIntent"
        },
        {
            "intent": "AMAZON.HelpIntent"
        },
        {
            "intent": "AMAZON.CancelIntent"
        },
        {
            "intent": "AMAZON.StopIntent"
        },
        {
            "intent": "StartMusicIntent"
        },
        {
            "intent": "PlayGenreIntent",
            "slots": [
              {
                "name": "MusicGenre",
                "type": "AMAZON.Genre"
              }
              ]
        },
        {
            "intent": "PlayArtistIntent",
            "slots": [
              {
                "name": "MusicArtist",
                "type": "AMAZON.MusicGroup"
              }
              ]
        },
        {
            "intent": "PlayAlbumIntent",
            "slots": [
              {
                "name": "MusicAlbum",
                "type": "AMAZON.MusicAlbum"
              }
              ]
        },
        {
            "intent": "PlaySongIntent",
            "slots": [
              {
                "name": "MusicSong",
                "type": "AMAZON.MusicRecording"
              }
              ]
        },
        {
            "intent": "PlayPlaylistIntent",
            "slots": [
              {
                "name": "MusicPlaylist",
                "type": "AMAZON.MusicPlaylist"
              }
              ]
        },
        {
            "intent": "GetAuthTokenIntent"
        }
    ]
}
