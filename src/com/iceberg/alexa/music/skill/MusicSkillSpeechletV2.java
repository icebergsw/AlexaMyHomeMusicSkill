package com.iceberg.alexa.music.skill;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.sql.SQLException;
import java.util.*;

import com.amazon.speech.json.SpeechletRequestEnvelope;
import com.amazon.speech.slu.Intent;
import com.amazon.speech.slu.Slot;
import com.amazon.speech.speechlet.*;
import com.amazon.speech.speechlet.interfaces.audioplayer.*;
import com.amazon.speech.speechlet.interfaces.audioplayer.directive.PlayDirective;
import com.amazon.speech.speechlet.interfaces.audioplayer.directive.StopDirective;
import com.amazon.speech.speechlet.interfaces.audioplayer.request.*;
import com.amazon.speech.speechlet.interfaces.playbackcontroller.PlaybackController;
import com.amazon.speech.speechlet.interfaces.playbackcontroller.request.*;
import com.amazon.speech.speechlet.interfaces.system.SystemInterface;
import com.amazon.speech.speechlet.interfaces.system.SystemState;
import com.amazon.speech.ui.LinkAccountCard;
import com.amazon.speech.ui.PlainTextOutputSpeech;
import com.amazon.speech.ui.SimpleCard;

/**
 * This is the My Home Music Alexa skill kit Java Lambda implementation (based on
 * the skill kit template).
 * 
 * This supports some custom intents and most audio player directives. See the
 * intentschema.txt and sampleutterances.txt files for details.
 * 
 * Even though most audio player directives are theoretically supported, many
 * will only work if the Lambda function has network access; if that's the
 * case, then vpcEnabled has to be set to true in ServerCommand.java. And you
 * have to be linked to a server implementation that does something with those
 * commands.
 *
 */
public class MusicSkillSpeechletV2 implements SpeechletV2, AudioPlayer, PlaybackController {

	public String protocol = "https://";
	
	// root is what the user defines when they link their account
	public String root = null;
	
	// auth token is the token linking the user to an account
	public String authToken = "";
	
	// we get the userid but we never actually use it
	public String userid;
	
	/**
	 * On launch, we just start random music playing.
	 * Note we only get this if a specific intent, with
	 * a more specific request, hasn't been provided.
	 * 
	 * We ping first, just to ensure we can get to the server.
	 */
	@Override
	public SpeechletResponse onLaunch(SpeechletRequestEnvelope<LaunchRequest> request) {
		try
		{
			determineUserRoot(request.getSession(), request.getContext());
			ServerCommand.executeAndWait(buildAuthUrl(Statics.PingUrl));
			// if you don't have any text here (blank string) it plays one song then stops... odd.
			return startPlayback(Statics.RandomSongUrl, "Playing random music");
		}
		catch (AccountLinkRequiredException ex)
		{
			return getLinkAccountCard();
		}
		catch (Exception ex)
		{
			return getDebugResponse(ex);
		}
	}

	/**
	 * This is called for our specific intents, but we're going to delegate to processIntent()...
	 */
	@Override
	public SpeechletResponse onIntent(SpeechletRequestEnvelope<IntentRequest> request) {
		try
		{
			determineUserRoot(request.getSession(), request.getContext());
			return processIntent(request.getRequest(), request.getSession());
		}
		catch (AccountLinkRequiredException ex)
		{
			return getLinkAccountCard();
		}
		catch (Exception ex)
		{
			return getDebugResponse(ex);
		}
	}

	/**
	 * Deal with our custom defined intents. The audio player and playback control
	 * intents go to specific functions in this class (based on the interfaces),
	 * so we just have to deal with the intents that we've defined, although that
	 * does include explicit audio (playback) control intents (such as when the
	 * user says "next").
	 * 
	 * @param request
	 * @param session
	 * @return
	 * @throws UnsupportedEncodingException 
	 */
	private SpeechletResponse processIntent(IntentRequest request, Session session) throws Exception {
        Intent intent = request.getIntent();
        String intentName = (intent != null) ? intent.getName() : null;

        
        if ("GetAuthTokenIntent".equals(intentName))
        {
        	return getAuthTokenViaCard(session);
        }
        if ("StartMusicIntent".equals(intentName))
        {
    		return startPlayback(Statics.RandomSongUrl, "Playing random songs");
        }
        else if ("PlayGenreIntent".equals(intentName))
        {
        	String genre = getSlotValue(intent, "MusicGenre");
        	return startPlayback(Statics.FilteredUrl + "?genre=" + genre, "Playing genre " + genre);
        }
        else if ("PlayArtistIntent".equals(intentName))
        {
        	String artist = getSlotValue(intent, "MusicArtist");
        	return startPlayback(Statics.FilteredUrl + "?artist=" + URLEncoder.encode(artist, "UTF-8"), "Playing music by " + artist);
        }
        else if ("PlayAlbumIntent".equals(intentName))
        {
        	String album = getSlotValue(intent, "MusicAlbum");
        	return startPlayback(Statics.FilteredUrl + "?album=" + album, "Playing album " + album);
        }
        else if ("PlaySongIntent".equals(intentName))
        {
        	String song = getSlotValue(intent, "MusicSong");
        	return startPlayback(Statics.FilteredUrl + "?song=" + song, "Playing song " + song);
        }
        else if ("PlayPlaylistIntent".equals(intentName))
        {
        	String playlist = getSlotValue(intent, "MusicPlaylist");
        	return startPlayback(Statics.FilteredUrl + "?playlist=" + URLEncoder.encode(playlist, "UTF-8"), "Playing playlist " + playlist);
        }
        else if ("AMAZON.PauseIntent".equals(intentName))
        {
        	return stopPlayback();
        }
        else if ("AMAZON.ResumeIntent".equals(intentName))
        {
        	return startPlayback(Statics.NextSongUrl, "");
        }
        else if ("AMAZON.NextIntent".equals(intentName))
        {
			return specialPlayback("cmd=next");
        }
        else if ("AMAZON.PreviousIntent".equals(intentName))
        {
			return specialPlayback("cmd=previous");
        }
        else if ("AMAZON.LoopOnIntent".equals(intentName))
        {
        	// try to hit the server, then send a null response
        	triggerServerUrl(Statics.LoopUrl + "/on");
        	return nullResponse();
        }
        else if ("AMAZON.LoopOffIntent".equals(intentName))
        {
        	// try to hit the server, then send a null response
        	triggerServerUrl(Statics.LoopUrl + "/off");
        	return nullResponse();
        }
        else if ("AMAZON.ShuffleOnIntent".equals(intentName))
        {
        	// try to hit the server, then send a null response
        	triggerServerUrl(Statics.ShuffleUrl + "/on");
        	return nullResponse();
        }
        else if ("AMAZON.ShuffleOffIntent".equals(intentName))
        {
        	// try to hit the server, then send a null response
        	triggerServerUrl(Statics.ShuffleUrl + "/off");
        	return nullResponse();
        }
        else if ("AMAZON.RepeatIntent".equals(intentName))
        {
        	// try to hit the server, then send a null response
        	triggerServerUrl(Statics.RepeatUrl);
        	return nullResponse();
        }
        else if ("AMAZON.StartOverIntent".equals(intentName))
        {
        	// try to hit the server, then send a null response
        	triggerServerUrl(Statics.StartOverUrl);
        	return nullResponse();
        }
        else if ("AMAZON.HelpIntent".equals(intentName))
        {
    		return getHelpResponse(session);
        }
        else if ("AMAZON.CancelIntent".equalsIgnoreCase(intentName))
        {
    		return getCancelResponse();
        }
        else if ("AMAZON.StopIntent".equalsIgnoreCase(intentName))
        {
    		return getCancelResponse();
        }
        else
        {
    		return getHelpResponse(session);
        }
	}

	private String getSlotValue(Intent intent, String slotName) {
		String slotValue = "";
		
		if (intent != null)
		{
			Slot slot = intent.getSlot(slotName);
			if (slot != null)
			{
				slotValue = slot.getValue();
			}
		}
		
		return slotValue;
	}

	/**
	 * Sends the user a card with their authentication token.
	 * This is in case they're using the AlexaMusicServer and want
	 * to include the auth token to authorize requests.
	 * 
	 * @param session
	 * @return
	 */
	private SpeechletResponse getAuthTokenViaCard(Session session) {
		PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
		speech.setText("Sending card.");
		
		SimpleCard card = new SimpleCard();
		card.setTitle("My Home Music");
		String content = "Your authentication token is: " +
				session.getUser().getAccessToken();
		card.setContent(content);
		
		return SpeechletResponse.newTellResponse(speech, card);
	}

	@Override
	public void onSessionStarted(SpeechletRequestEnvelope<SessionStartedRequest> request) {
		// nothing to do here
		// most music player requests don't include session information anyway
	}

	@Override
	public void onSessionEnded(SpeechletRequestEnvelope<SessionEndedRequest> request) {
		// nothing to do here
	}

	@Override
	public SpeechletResponse onPlaybackFailed(SpeechletRequestEnvelope<PlaybackFailedRequest> request) {
		// if playback fails, it fails
		// do nothing more specific
		// just tell the user
		PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
		speech.setText("I'm sorry, but playback failed. Please try again.");
		SpeechletResponse response = SpeechletResponse.newTellResponse(speech);
		return response;
	}

	@Override
	public SpeechletResponse onPlaybackFinished(SpeechletRequestEnvelope<PlaybackFinishedRequest> request) {
		// basically do nothing
		return nullResponse();
	}

	/**
	 * In this method, we queue up the next song to play.
	 * For our music player, that's always "nextsong" because whatever
	 * started music playback should have set up the correct song list.
	 * 
	 * Other than the methods that start a new song playing, this is really
	 * the main method that will get called again and again by the Alexa
	 * service in order to keep playing songs.
	 */
	@Override
	public SpeechletResponse onPlaybackNearlyFinished(SpeechletRequestEnvelope<PlaybackNearlyFinishedRequest> request) {
		try
		{
			determineUserRoot(request.getSession(), request.getContext(), request.getRequest().getToken());

			return continuePlayback(request.getRequest().getToken());
		}
		catch (Exception ex)
		{
			return getDebugResponse(ex);
		}
	}

	@Override
	public SpeechletResponse onPlaybackStarted(SpeechletRequestEnvelope<PlaybackStartedRequest> request) {
		// basically do nothing
		return nullResponse();
	}

	@Override
	public SpeechletResponse onPlaybackStopped(SpeechletRequestEnvelope<PlaybackStoppedRequest> request) {
		// basically do nothing
		return nullResponse();
	}

	/**
	 * User pushed the next button on the controller.
	 */
	@Override
	public SpeechletResponse onNextCommandIssued(SpeechletRequestEnvelope<NextCommandIssuedRequest> request) {
		try
		{
			determineUserRoot(request.getSession(), request.getContext());
			return specialPlayback("cmd=next");
		}
		catch (Exception ex)
		{
			return getDebugResponse(ex);
		}
	}

	/**
	 * User pushed the pause button on the controller.
	 */
	@Override
	public SpeechletResponse onPauseCommandIssued(SpeechletRequestEnvelope<PauseCommandIssuedRequest> request) {
		return stopPlayback();
	}

	/**
	 * User pushed the play button on the controller.
	 */
	@Override
	public SpeechletResponse onPlayCommandIssued(SpeechletRequestEnvelope<PlayCommandIssuedRequest> request) {
		try
		{
			determineUserRoot(request.getSession(), request.getContext());
			return startPlayback(Statics.NextSongUrl, "");
		}
		catch (Exception ex)
		{
			return getDebugResponse(ex);
		}
	}

	/**
	 * User pushed the previous button on the controller.
	 */
	@Override
	public SpeechletResponse onPreviousCommandIssued(SpeechletRequestEnvelope<PreviousCommandIssuedRequest> request) {
		try
		{
			determineUserRoot(request.getSession(), request.getContext());
			return specialPlayback("cmd=previous");
		}
		catch (Exception ex)
		{
			return getDebugResponse(ex);
		}
	}

	/**
	 * Called to begin playback of a new stream of songs.
	 * 
	 * @param songUrl
	 * @param message
	 * @return
	 */
	private SpeechletResponse startPlayback(String songUrl, String message) {
		PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
		speech.setText(message);
		SpeechletResponse response = SpeechletResponse.newTellResponse(speech);
		
		Stream s = new Stream();
		String playbackToken = getNextToken(null);
		s.setToken(playbackToken);
		s.setUrl(withSequence(buildAuthUrl(songUrl), playbackToken));
		s.setOffsetInMilliseconds(0L);
		s.setExpectedPreviousToken(null);
		
		AudioItem ai = new AudioItem();
		ai.setStream(s);
		
		PlayDirective play = new PlayDirective();
		play.setPlayBehavior(PlayBehavior.REPLACE_ALL);
		play.setAudioItem(ai);

		List<Directive> directives = new ArrayList<>();
		directives.add(play);
		
		response.setDirectives(directives);
		
		return response;
	}

	/**
	 * Called to continue playing back music. This is basically just called
	 * by onPlaybackNearlyFinished.
	 * 
	 * @param playbackToken
	 * @return
	 */
	private SpeechletResponse continuePlayback(String playbackToken) {
		SpeechletResponse response = new SpeechletResponse();
		
		Stream s = new Stream();
		String nextToken = getNextToken(playbackToken);
		s.setToken(nextToken);
		s.setUrl(withSequence(buildAuthUrl(Statics.NextSongUrl), nextToken));
		s.setOffsetInMilliseconds(0L);
		s.setExpectedPreviousToken(playbackToken);
		
		AudioItem ai = new AudioItem();
		ai.setStream(s);
		
		PlayDirective play = new PlayDirective();
		play.setPlayBehavior(PlayBehavior.ENQUEUE);
		play.setAudioItem(ai);

		List<Directive> directives = new ArrayList<>();
		directives.add(play);
		
		response.setDirectives(directives);
		
		return response;
	}

	/**
	 * Called when previous or next is selected. When this is triggered,
	 * we don't know the prior playback token, so we're restarting the
	 * sequence from this end, but passing a special URL parameter to
	 * indicate previous or next.
	 * 
	 * @param queryParameter A special parameter to include in the
	 * playback url of the form key=value.
	 * @return
	 */
	private SpeechletResponse specialPlayback(String queryParameter) {
		SpeechletResponse response = new SpeechletResponse();
		
		Stream s = new Stream();
		String playbackToken = getNextToken(null);
		s.setToken(playbackToken);
		s.setUrl(withQueryParameter(buildAuthUrl(Statics.NextSongUrl), queryParameter));
		s.setOffsetInMilliseconds(0L);
		s.setExpectedPreviousToken(null);
		
		AudioItem ai = new AudioItem();
		ai.setStream(s);
		
		PlayDirective play = new PlayDirective();
		play.setPlayBehavior(PlayBehavior.REPLACE_ALL);
		play.setAudioItem(ai);

		List<Directive> directives = new ArrayList<>();
		directives.add(play);
		
		response.setDirectives(directives);
		
		return response;
	}

	private SpeechletResponse stopPlayback() {
		PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
		speech.setText("");
		SpeechletResponse response = SpeechletResponse.newTellResponse(speech);
		
		StopDirective stop = new StopDirective();

		List<Directive> directives = new ArrayList<>();
		directives.add(stop);
		
		response.setDirectives(directives);
		
		return response;
	}

	private SpeechletResponse getHelpResponse(Session session)
	{
		PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
		speech.setText("Open the service to start your music playing, then use the normal Alexa music controls." +
				"Say play music by artist name to play music by an artist." +
				"Or say, play rock music, for example, to play music of a particular genre.");

		return SpeechletResponse.newTellResponse(speech);
	}

	private SpeechletResponse getCancelResponse() {
		PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
		speech.setText("");
		return SpeechletResponse.newTellResponse(speech);
	}

	private SpeechletResponse getDebugResponse(Exception ex) {
		return getCardResponse("Sorry, the skill has encountered an error.",
				"My Home Music Error",
				"We're sorry, but My Home Music encountered an error. Please try again later.");
		//return getCardResponse("exception", "exception", ex.getMessage());
	}

	private SpeechletResponse getCardResponse(String voice, String cardTitle, String cardText) {
		PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
		speech.setText(voice != null ? voice : "");
		
		SimpleCard card = new SimpleCard();
		card.setTitle(cardTitle);
		card.setContent(cardText);
		
		return SpeechletResponse.newTellResponse(speech, card);
	}

	private SpeechletResponse nullResponse() {
		PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
		speech.setText("");
		return SpeechletResponse.newTellResponse(speech);
	}

	/**
	 * Triggered when the user tries to do just about anything but account
	 * linking hasn't completed yet. This is required by the Alexa interface
	 * (at least rules for getting an app published).
	 * 
	 * @return
	 */
	private SpeechletResponse getLinkAccountCard() {
		PlainTextOutputSpeech speech = new PlainTextOutputSpeech();
		speech.setText("Please link your account to your home server location to use this skill.");
		
		LinkAccountCard card = new LinkAccountCard();
		
		return SpeechletResponse.newTellResponse(speech, card);
	}

	/**
	 * Called to hit a particular server URL, plus root.
	 * This is used to send commands to a server, if the Lambda
	 * function can make outgoing calls, and the server is configured
	 * to receive them.
	 * 
	 * @param startoverurl
	 * @throws Exception 
	 */
	private void triggerServerUrl(String command) throws Exception {
		ServerCommand.executeAsync(buildAuthUrl(command));
	}

	/**
	 * The next token is just a one-up integer from the prior song,
	 * with the root prepended to it.
	 * 
	 * If we can't successfully parse the prior token, return "1".
	 * 
	 * @param token
	 * @return
	 */
	private String getNextToken(String token) {
		if (token != null)
		{
			try
			{
				int next = getNextTokenSequence(token);
				return getTokenForSequence(next);
			}
			catch (Exception ex)
			{
				// fall through to end of method
			}
		}
		
		return root + "#1";
	}

	private String getTokenForSequence(int seq) {
		return root + "#" + Integer.toString(seq);
	}
	
	private int getNextTokenSequence(String token) {
		try
		{
			int current = Integer.parseInt(token.split("#")[1]);
			int next = current+1;
			return next;
		}
		catch (Exception ex)
		{
			return 1;
		}
	}

	private String parseRootFromPlaybackToken(String playbackToken) {
		if (playbackToken != null)
		{
			return playbackToken.split("#")[0];
		}
		
		return null;
	}

	/**
	 * See the version with another parameter...
	 * 
	 * @param session
	 * @param context
	 * @throws SQLException
	 */
	private void determineUserRoot(Session session, Context context) throws SQLException {
		determineUserRoot(session, context, null);
	}

	/**
	 * This is the core function called to determine the https root path
	 * for the user. We mostly do that by correlating entries in the database
	 * based on account linking. But we can store the information in the
	 * session or the token, also, so that we don't have to lookup the
	 * value from the database for every single call.
	 * 
	 * @param session
	 * @param context
	 * @throws SQLException
	 */
	private void determineUserRoot(Session session, Context context, String token) throws SQLException {
		getUserId(session, context);
		getAuthTokenIfAvailable(session);

		getUserRootVariousWays(session, context, token);
		
		if (root == null)
		{
			throw new AccountLinkRequiredException();
		}
		
		setUserRootInSession(session);
	}

	/**
	 * Called to get the user ID for the user for this application.
	 * Even though we never use it, at least not in the current implementation.
	 * 
	 * @param session
	 * @param context
	 */
	private void getUserId(Session session, Context context) {
		getUserIdFromSession(session);
		
		if ((userid == null) && (context != null))
		{
			getUserIdFromContext(context);
		}
	}

	private void getUserIdFromSession(Session session) {
		if (session != null)
		{
			User user = session.getUser();
			if (user != null)
			{
				userid = user.getUserId();
			}
		}
	}

	private void getUserIdFromContext(Context context) {
		if (context != null)
		{
			SystemState st = (SystemState) context.getState(SystemInterface.class, SystemInterface.STATE_TYPE);
			if (st != null)
			{
				User user = st.getUser();
				if (user != null)
				{
					userid = user.getUserId();
				}
			}
		}
	}

	/**
	 * If provided, an authentication token comes from the session.
	 * @param session
	 */
	private void getAuthTokenIfAvailable(Session session) {
		if (session != null)
		{
			User user = session.getUser();
			if (user != null)
			{
				authToken = user.getAccessToken();
			}
		}
	}

	/**
	 * The session could contain a root attribute.
	 * The system could have set an env var.
	 * We could read from the database.
	 * Or we could get it via playback token, which is formatted to include it.
	 * 
	 * @param session
	 * @param context
	 * @param playbackToken
	 * @throws SQLException
	 */
	private void getUserRootVariousWays(Session session, Context context, String playbackToken) throws SQLException {
		// first see if it's part of the session
		if (session != null)
		{
			root = (String)session.getAttribute("mhm_root");
		}
		
		if ((root == null) || (root.isEmpty()))
		{
			// then try to get the root from env var
			root = System.getenv("MHM_ROOT");
			if ((root == null) || (root.isEmpty()))
			{
				root = parseRootFromPlaybackToken(playbackToken);
				if ((root == null) || (root.isEmpty()))
				{
					// otherwise, read from the database
					root = DBClient.getRootFromToken(authToken);
				}
			}
		}
		
	}

	/**
	 * Most commands don't have a session, and most audio playback commands
	 * require that the session close with the audio playback directive. So
	 * this doesn't have a lot of value. But we do so to be a good idea, and
	 * in case this changes in the future and Alexa adds sensible session
	 * handling to the audio playback interface.
	 * @param session
	 */
	private void setUserRootInSession(Session session) {
		if (session != null)
		{
			session.setAttribute("mhm_root", root);
		}
	}

	/**
	 * Creates the URL for calling the server.
	 * The URL must be https for protocol, and then contains
	 * the user root (defined with account linking or MHM_ROOT)
	 * followed by the actual server URL, and possibly the auth
	 * token if that's present.
	 * 
	 * @param customPart
	 * @return
	 */
	private String buildAuthUrl(String customPart)
	{
		String authPart = (customPart.contains("?")) ? "&authToken=" : "?authToken=";
		
		return protocol + root + customPart + authPart + authToken;
	}

	/**
	 * Adds the sequence number for the desired song to the URL.
	 * 
	 * @param baseUrl A fully-formed URL, we're just appending to the end of it.
	 * @param parameter A query parameter in the form key=value.
	 * @return
	 */
	private String withQueryParameter(String baseUrl, String parameter) {
		String separator = (baseUrl.contains("?")) ? "&" : "?";
		
		return baseUrl + separator + parameter;
	}

	/**
	 * Adds the sequence number for the current token to the URL.
	 * 
	 * @param baseUrl
	 * @param token Current opaque token in the form: root#n, where
	 * root is the URL root path, e.g. something.net, the # is literal,
	 * and "n" is the current sequence number.
	 * @return
	 */
	private String withSequence(String baseUrl, String token) {
		String seqPart = (baseUrl.contains("?")) ? "&seq=" : "?seq=";
		
		int seq = Integer.parseInt(token.split("#")[1]);
		String sequenceNumber = Integer.toString(seq);
		
		return baseUrl + seqPart + sequenceNumber;
	}

}
