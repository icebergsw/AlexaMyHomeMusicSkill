package com.iceberg.alexa.music.skill;

import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.security.SecureRandom;
import java.security.cert.X509Certificate;

import javax.net.ssl.*;
import javax.net.ssl.X509TrustManager;

/**
 * Called to run a command on a server by calling https
 * on it.
 * 
 * This class has to handle the https server connection.
 * We ignore server certs - we're not getting data back
 * so we just trust any server.
 * 
 * We run the call in the background because we don't care
 * if it passes or fails.
 *
 * Important: Read the notes on vpcEnabled value.
 * 
 * Note: This class is effectively untested. The code is mostly
 * copied from code that presumably is working, but I hadn't
 * tested this specific class outside of Lambda, and the Lambda
 * function it's being used in (my home music) doesn't have
 * network access. So if you decide to use this class, you
 * will have to test it.
 */
public class ServerCommand
{
	/**
	 * NOTE: The default value of this static is false.
	 * That's because the default AWS Lambda behavior is to deny
	 * network access to a lambda function. It needs to be added
	 * to a VPC that allows network access in order to talk to
	 * the network. Until that's done these functions can't reach
	 * the network and will cause the service to fail.
	 * 
	 * If this is installed as a lambda function with network
	 * access, these could be enabled (by changing this static).
	 * 
	 * TODO: could instead change this to an environment variable
	 * on the service itself.
	 */
	private static final boolean vpcEnabled = false;
	
	/**
	 * Executes a call in the background without waiting for a response.
	 * 
	 * For most server commands, the Alexa service doesn't get a response;
	 * there's nothing to send back. So there's no point in waiting for
	 * a response - just let it run.
	 * 
	 * @param commandUrl
	 * @throws Exception
	 */
	public static void executeAsync(String commandUrl) throws Exception {
		if (vpcEnabled)
		{
			disableCertificateValidation();
			
		    final URL myurl = new URL(commandUrl);
		    
		    // run in a background thread, we don't want to wait
		    // because we don't care if it passes or fails
		    new Thread() {
		    	public void run()
		    	{
					try {
			    	    HttpsURLConnection con = (HttpsURLConnection)myurl.openConnection();
			    	    try (InputStream ins = con.getInputStream())
			    	    {
			    	    	
			    	    }
					} catch (IOException e) {
						// ignore
					}
		    	}
		    }.start();
		}
	}

	/**
	 * Executes and waits for a response.
	 * 
	 * @param commandUrl
	 * @throws Exception
	 */
	public static void executeAndWait(String commandUrl) throws Exception {
		if (vpcEnabled)
		{
			disableCertificateValidation();
			
		    final URL myurl = new URL(commandUrl);
		    
		    HttpsURLConnection con = (HttpsURLConnection)myurl.openConnection();
		    try (InputStream ins = con.getInputStream())
		    {
		    	// just waiting for it to finish, as opposed to getting an exception
		    }
		}
	}
	
	/**
	 * We disable certificate validation so that any https site will
	 * be accepted.
	 */
	 public static void disableCertificateValidation() {
		    // Create a trust manager that does not validate certificate chains
		    TrustManager[] trustAllCerts = new TrustManager[] { 
		      new X509TrustManager() {
		        public X509Certificate[] getAcceptedIssuers() { 
		          return new X509Certificate[0]; 
		        }
		        public void checkClientTrusted(X509Certificate[] certs, String authType) {}
		        public void checkServerTrusted(X509Certificate[] certs, String authType) {}
		    }};

		    // Ignore differences between given hostname and certificate hostname
		    HostnameVerifier hv = new HostnameVerifier() {
		      public boolean verify(String hostname, SSLSession session) { return true; }
		    };

		    // Install the all-trusting trust manager
		    try {
		      SSLContext sc = SSLContext.getInstance("SSL");
		      sc.init(null, trustAllCerts, new SecureRandom());
		      HttpsURLConnection.setDefaultSSLSocketFactory(sc.getSocketFactory());
		      HttpsURLConnection.setDefaultHostnameVerifier(hv);
		    } catch (Exception e) {}
		  }
}
