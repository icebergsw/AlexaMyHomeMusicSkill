package com.iceberg.alexa.music.skill;

/**
 * Doesn't need to do anything special - just a trigger that account
 * linking is required.
 *
 */
public class AccountLinkRequiredException extends RuntimeException {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

}
