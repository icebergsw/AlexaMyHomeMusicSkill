package com.iceberg.alexa.music.skill;

public class Statics {

	// normal song play urls
	// no additional data is passed (other than auth token)
	public static final String RandomSongUrl = "/randomsong.mp3";
	public static final String NextSongUrl = "/nextsong.mp3";
	public static final String PreviousSongUrl = "/previoussong.mp3";

	// start a session filtered to something in particular, has additional query params
	public static final String FilteredUrl = "/filteredsong.mp3";

	// issue commands to the server
	// must set vpcEnable to true in ServerCommand.java for these to be called
	public static final String LoopUrl = "/command/loop";
	public static final String ShuffleUrl = "/command/shuffle";
	public static final String RepeatUrl = "/command/repeat";
	public static final String StartOverUrl = "/command/startover";
	
	// ping the server just to test the connection
	// must set vpcEnable to true in ServerCommand.java for this to ever be called
	public static final String PingUrl = "/ping";
}
