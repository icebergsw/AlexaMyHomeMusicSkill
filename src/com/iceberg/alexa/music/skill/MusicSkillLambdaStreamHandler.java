package com.iceberg.alexa.music.skill;

import java.util.HashSet;
import java.util.Set;

import com.amazon.speech.speechlet.lambda.SpeechletRequestStreamHandler;

/**
 * Basic (effectively default implementation) stream handler for Java
 * for an Alexa skill.
 * 
 * If MHM_SKILL_ID environment variable (in the lambda configuration) is
 * set, this lambda service will only process requests from that Alexa
 * skill. If not set, any application could (if they could find it) call
 * this service.
 *
 */
public class MusicSkillLambdaStreamHandler extends SpeechletRequestStreamHandler {
    private static final Set<String> supportedApplicationIds;

    static {
        /*
         * This Id can be found on https://developer.amazon.com/edw/home.html#/ "Edit" the relevant
         * Alexa Skill and put the relevant Application Ids in this Set.
         */
        supportedApplicationIds = new HashSet<String>();
        // supportedApplicationIds.add("amzn1.echo-sdk-ams.app.[unique-value-here]");
        String skillId = System.getenv("MHM_SKILL_ID");
        if (skillId != null) {
        	supportedApplicationIds.add(skillId);
        }
    }

    public MusicSkillLambdaStreamHandler() {
        super(new MusicSkillSpeechletV2(), supportedApplicationIds);
    }

    public static void main(String[] args)
    {
    	// placeholder only so we can use the eclipse runnable jar export
    }

}
