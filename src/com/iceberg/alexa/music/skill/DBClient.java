package com.iceberg.alexa.music.skill;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * This class reads from the database.
 * 
 * The My Home Music database is a single Amazon RDS database.
 * 
 * The database name is myhomemusic. The only table is user_tokens.
 * The columns are: token, https_root
 * 
 * The table is populated by the account linking service.
 * 
 * If one wants to skip using the database to lookup roots for
 * users from tokens (probably to do a single instance install),
 * then set the env var MHM_ROOT to the desired value in the
 * lambda configuration and this class will never be used.
 *
 */
public class DBClient {

	private static final String connUrl = getConnection();
	private static final String username = getUser();
	private static String password = getPassword();
	
	public static String getRootFromToken(String token) throws SQLException {
		try (Connection conn = DriverManager.getConnection(connUrl, username, password))
		{
			if (token != null)
			{
				return readRootFromConnectionForToken(conn, token);
			}
		}
		
		return null;
	}

	private static String readRootFromConnectionForToken(Connection conn, String token) throws SQLException {
		try (PreparedStatement stmt = conn.prepareStatement("select https_root from myhomemusic.user_tokens where token = ?"))
		{
			stmt.setString(1, token);
			
			try (ResultSet rs = stmt.executeQuery())
			{
				if (rs.next())
				{
					return rs.getString(1);
				}
			}
		}
		
		// have to return null; otherwise (like with an exception)
		//  things will fail if the user had a token, but got disconnected
		return null;
	}

	static String getConnection() {
		return System.getenv("MHM_CONNECTION");
	}

	static String getUser() {
		return System.getenv("MHM_USER");
	}

	// TODO - add encryption via AWS (Role needs encrypt/decrypt)
	static String getPassword() {
		return System.getenv("MHM_PASSWORD");
	}

	// this is the code AWS provides for decrypting keys from env vars
	// although I don't know where to get these classes
//    private static String decryptKey() {
//        byte[] encryptedKey = Base64.getDecoder().decode(System.getenv("MHM_PASSWORD"));
//
//        AWSKMS client = AWSKMSClientBuilder.defaultClient();
//
//        DecryptRequest request = new DecryptRequest()
//                .withCiphertextBlob(ByteBuffer.wrap(encryptedKey));
//
//        ByteBuffer plainTextKey = client.decrypt(request).getPlaintext();
//        return new String(plainTextKey.array(), Charset.forName("UTF-8"));
//    }

}
