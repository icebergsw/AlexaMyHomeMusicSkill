## Synopsis

Provides the My Home Music Alexa skills lambda function implementation.

This is the lambda function for an Alexa skill: the My Home Music skill. It allows one to play music from their own home computer on their own Amazon Echo device.

This lambda implementation is provided for anyone who would like to implement the skill for themselves (rather than using a published skill). This requires some knowledge of how to create Alexa skills via the Alexa developer console and publish Lambda functions via AWS.

See the [My Home Music help page](https://icebergsw.com/alexa/myhomemusic/mhmhelp.html) for additional information. 

## Code Example

This project requires a Java 8 runtime and the Alexa Java Skills Kit.

## Motivation

Amazon Echo devices are designed to play music. There are generally these existing models:

1. Play music from your Amazon cloud library over your Echo device.
2. Stream music from an existing music streaming service to which you have an account over your Echo device.
3. Control your home audio configuration via voice-commands from Echo to play your music via your home entertainment system (smart home skills linking).

What is missing is:

4. Play your own music, from your own home server, over the Echo device.

The My Home Music application is intended to fill this last part. This project provides the Alexa skill implementation (primarily the lambda function), including the intent schema and sample utterances.

## Installation

Clone the project and compile as a runnable jar file including the Alexa Java Skills Kit. Or compile in any other method suitable for publication as an AWS Java Lambda function.

## API Reference

This is an AWS Lambda function.

See the [My Home Music help page](https://icebergsw.com/alexa/myhomemusic/mhmhelp.html) for additional information about the corresponding server that must be available.

The intent schema and the sample utterances must correspond to those in the project.

When used by itself (standalone user case) the Lambda function should have the environment variable `MHM_ROOT` set to the address and port of the user's home server (the endpoint accessible to the internet that is probably port forwarded to a home computer).

The `MHM_SKILL_ID` environment variable for the Lambda function should be set to the skill ID of the user's Alexa skill in order to ensure that only the user's skill can execute commands via the Lambda function.

## Tests

N/A

## Contributors

This skill (lambda) implementation must remain in sync with the Alexa My Home Music server implementation.

The intent schema and sample utterances should also be kept in sync.

The published version of this expects account linking of the Alexa skill and data storage in an RDS database. Those are not required for personal use; instead the `MHM_ROOT` environment variable would be set on the Lambda function in AWS.

## License

This software is free-to-use and free-to-modify and otherwise free to do with as one pleases. I haven't (yet) picked a specific license implementation for it.
